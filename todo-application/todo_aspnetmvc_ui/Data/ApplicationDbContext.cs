﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using todo_domain_entities.Data;

namespace todo_aspnetmvc_ui.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<TodoList>(b =>
            {
                b.Property("IsHidden")
                .IsRequired()
                .HasDefaultValue(false)
                .HasColumnType("bit");
            });
        }

        public DbSet<TodoList> TodoList { get; set; }
    }
}