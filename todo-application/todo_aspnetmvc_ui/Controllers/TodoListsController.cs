﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using todo_aspnetmvc_ui.Data;
using todo_domain_entities.Data;

namespace todo_aspnetmvc_ui.Controllers
{
    public class TodoListsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public TodoListsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: TodoLists
        [Authorize]
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.TodoList.Include(t => t.ApplicationUser);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: TodoLists/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.TodoList == null)
            {
                return NotFound();
            }

            var todoList = await _context.TodoList
                .Include(t => t.ApplicationUser)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (todoList == null)
            {
                return NotFound();
            }

            return View(todoList);
        }

        // GET: TodoLists/Create
        [Authorize]
        public IActionResult Create(string vtitle)
        {
            ViewBag.ValueForTitle = vtitle;
            var vm = new TodoList();
            vm.StatusStates = new List<SelectListItem>
            {
                new SelectListItem { Text = "Not Started", Value = "0" },
                new SelectListItem { Text = "In Progress", Value= "1" },
                new SelectListItem { Text = "Completed", Value = "2" }
            };

            return View(vm);
        }

        // POST: TodoLists/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Title,Description,DueDate,State,IsHidden")] TodoList todoList)
        {
            var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);

            if (ModelState.IsValid)
            {
                ApplicationUser user = await _context.Users.FindAsync(userid);
                var newTodoList = new TodoList()
                {
                    ApplicationUser = user,
                    Title = todoList.Title,
                    Description = todoList.Description,
                    CreatedAt = DateTime.Now,
                    UpdatedAt = DateTime.Now,
                    DueDate = todoList.DueDate,
                    State = todoList.State,
                    UserId = userid
                };

                _context.Add(newTodoList);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(todoList);
        }

        public void ChangeStatus()
        {

        }

        public void Hide()
        {

        }
        // GET: TodoLists/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.TodoList == null)
            {
                return NotFound();
            }

            var todoList = await _context.TodoList.FindAsync(id);
            if (todoList == null)
            {
                return NotFound();
            }
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Id", todoList.UserId);
            return View(todoList);
        }

        // POST: TodoLists/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Title,Description,DueDate,State,IsHidden,CreatedAt,UpdatedAt,UserId")] TodoList todoList)
        {
            if (id != todoList.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(todoList);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TodoListExists(todoList.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Id", todoList.UserId);
            return View(todoList);
        }

        // GET: TodoLists/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.TodoList == null)
            {
                return NotFound();
            }

            var todoList = await _context.TodoList
                .Include(t => t.ApplicationUser)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (todoList == null)
            {
                return NotFound();
            }

            return View(todoList);
        }

        // POST: TodoLists/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.TodoList == null)
            {
                return Problem("Entity set 'ApplicationDbContext.TodoList'  is null.");
            }
            var todoList = await _context.TodoList.FindAsync(id);
            if (todoList != null)
            {
                _context.TodoList.Remove(todoList);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TodoListExists(int id)
        {
          return (_context.TodoList?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
