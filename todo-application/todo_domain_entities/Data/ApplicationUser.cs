﻿using Microsoft.AspNetCore.Identity;
using todo_domain_entities.Data;

namespace todo_domain_entities.Data
{
    public class ApplicationUser : IdentityUser
    {
        public ICollection<TodoList> TodoList { get; set; }
    }
}
