﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Mvc.Rendering;
using todo_domain_entities.Data;

namespace todo_domain_entities.Data
{
    [Table("TodoList")]
    public class TodoList
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [MaxLength(50)]
        public string Title { get; set; } = null!;
        public string Description { get; set; } = null!;
        [Column("Due Date")]
        public DateTime DueDate { get; set; }
        [NotMapped]
        [Column("Status")]
        public IEnumerable<SelectListItem> StatusStates { get; set; }
        public int State { get; set; }
        public bool IsHidden { get; set; }
        [Column("Created At")]
        public DateTime CreatedAt { get; set; }
        [Column("Updated At")]
        public DateTime UpdatedAt { get; set; }
        [Column("UserId")]
        public string UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual ApplicationUser ApplicationUser { get; set; } = null!;
    }
}
